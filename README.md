NewralNet
=============
A lightweight, easy to use and open source Java library for experimenting with
feed-forward neural nets and deep learning. 

The Project was built from scratch in Java and only uses [Apaches Commons Math Library](http://commons.apache.org/proper/commons-math/) 
for faster matrix and vector operations. Because it is built completely in Java, it is easy to observe and understand 
how the library works and possible to tinker around with it freely.

I originally developed this library for a school project and to get used to neural nets.
Because of that I was still learning to use neural net when I wrote this library so it might still contain some flaws.<br>
Since I wrote this library alone and have no experience with writing code for a GPU, this project is not GPU-Optimized
and thus not as fast as state-of-the-art deep learning libraries.

## Features
* Supports many feed-forward configurations because of ModularNet class that allows for any combination of layers
* Support for many commonly used feed-forward layer types:
  1. Fully connected
  2. Convolutional
  3. Pooling
  4. ReLU
  5. Softmax
* Support for two commonly used cost/error functions:
  1. Mean squared cost
  2. Cross entropy Cost
* Support for two commonly used optimizers:
  1. Stochastic gradient descent
  2. [Adam](https://arxiv.org/abs/1412.6980)
* Easily customizable and expendable due to a number of useful abstract classes that for new layers, cost functions or optimizers to be easily integrated
* Multithreading support for fast training
* BraInsight window that lets you observe and tinker around with any net (containing only Fully Connected, ReLU and Softmax Layers)

## How to use
Below is an example which trains a net capable of reproducing the AND-Function.
For an indepth documentation of the library please refer to the [Javadoc](https://flimmerkiste.gitlab.io/NewralNet/).
```java
final int BATCH_SIZE = 100, TRAINING_STEPS = 10000;
//Construct net with input size, optimizer, cost function and layers
ModularNet net = new ModularNet(2, 1, true, new AdamOptimizer(), new MeanSquaredCost(), 
	new FullyConnectedLayer(10), new ReLULayer(),
	new FullyConnectedLayer(1)
);
//Initialize batch arrays for the inputs and the corresponding labels
RealMatrix[] inputs = new RealMatrix[BATCH_SIZE], labels = new RealMatrix[BATCH_SIZE];
for(int s=0; s<TRAINING_STEPS; s++){
	//Fill batch-arrays with new input-label pairs
	for(int bi=0; bi<BATCH_SIZE; bi++){
		int a = (int)(Math.random() * 2);
		int b = (int)(Math.random() * 2);
		//Input: The two input variables of the and function
		inputs[bi] = new Array2DRowRealMatrix(new double[]{a,b});
		//Label: The correct answer: a && b (Converted to boolean and back)
		int label = (a==1) && (b==1) ? 1:0;
		labels[bi] = new Array2DRowRealMatrix(new double[]{label});
	}
	//Execute a single training step over the labeled inputs with 8 threads
	NewralNet.train(inputs, labels, net, 8);
	//Display progress
	if(s%1000 == 0)System.out.println("Progress: "+s/100+"%");
}
//Open BraInsight window to display results
new BraInsight(net);
```
## BraInsight
The BraInsight window lets you observe and tinker around with any net.

 * Nodes can be focused and moved with the left mouse button.
 * The view can be shifted with the right mouse button and zoomed with the mouse wheel
 * The view can be reset using the space bar
 * It is also possible to enter a custom input input into the net by focusing one or more input nodes and typing the desired input

The net must only contain fully connected, ReLU and Softmax layers

![BraInsight Window](http://i.imgur.com/q1Tj235.png)
*A BraInsight window displaying a net.*

The circles represent nodes and the lines between them the weights connecting them.<br>
The edge of a circle displays the bias of the node, the left part the input and the right part the output.<br>
A red color represent a positive and a blue color a negative value.

## How to install
Download [NewralNet.zip](../release/NewralNet.zip) and add the two jars to the build path of your project

## Todo
- [x] Write Library
- [x] Write Documentation & Generate Javadoc
- [x] Finish README
- [x] Softmax Layer
- [ ] More tests to find bugs
- [x] Generate release zip
- [ ] Multithreading Predictions
- [ ] BraInsight support for more net types
- [ ] ModularNet implements StringSaveAndLoadable
