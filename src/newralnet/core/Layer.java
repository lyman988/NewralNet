package newralnet.core;

import org.apache.commons.math3.linear.RealMatrix;

import newralnet.net.ModularNet;

/**
 * An abstract class describing a layer of a {@link ModularNet}
 */
public abstract class Layer implements StringSaveAndLoadable{
	//In&Out Matrices !Not Always Set!
	/**
	 * Matrix containing the latest inputs of the layer. <br>
	 * This value changes everytime new inputs are passed forward through this layer.
	 */
	public RealMatrix in;
	/**
	 * Matrix containing the latest outputs of the layer. <br>
	 * This value changes everytime new inputs are passed forward through this layer.
	 */
	public RealMatrix out;
	//Input Dimensions !Not Always Set!
	/**
	 * Expected Column Dimension of the input matrix
	 */
	public int inCD;
	/**
	 * Expected Row Dimension of the input matrix
	 */
	public int inRD;
	
	/**
	 * Passes the information through this layer
	 * @param in - The inputs of this layer (= the outputs of the previous layer)
	 * @return The outputs of this layer (= the result of passing the inputs through this layer)
	 */
	public abstract RealMatrix passForward(RealMatrix in);
	/**
	 * Passes the error back through this layer and calculates the derivatives of the cost function with respect to this layers inputs
	 * @param in - The error of the following layer (= the derivatives of the cost function with respect to the inputs of the following layer)
	 * @return The derivatives of the cost function with respect to this layers inputs
	 */
	public abstract RealMatrix passBackward(RealMatrix in);
	
	/**
	 * Initializes the layer
	 * @param inColumnDimension -  Column dimension of the expected inputs
	 * @param inRowDimention - Row dimension of the expected inputs
	 */
	public void initialize(int inColumnDimension, int inRowDimention){
		inCD=inColumnDimension;
		inRD=inRowDimention;
	}
	
	/**
	 * @return A deep copy of this layer
	 */
	public abstract Layer copy();
	
	/**
	 * @return The column dimension of the outputs
	 */
	public abstract int getOutColumnDimension();
	/**
	 * @return The row dimension of the expected inputs
	 */
	public abstract int getOutRowDimension();
}